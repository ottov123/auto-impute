# README #

auto-impute is a python script to automate the preparation of GWAS data, in plink format, for upload to the HRC Imputation website on the Michigan Imputation Server (https://imputationserver.sph.umich.edu/index.html)


### How do I get set up? ###

* Summary of set up
    * The script uses default python setup plus DRMAA library 
    * Must have plink and SHAPEIT installed and a reference haplotype genome downloaded
    
* Configuration
    * edit file to set path of binaries (plink, SHAPEIT)

* How to run tests
    * getDRMAAInfo.py - Test DRMAA loading

### What does it do? ###
1. Makes a snplist of ambiguous SNPs using *grep*
2. Uses *plink* to remove SNPs, set **0.05 threshold for SNP missingness**, and cut plink file by chromosome (a requirement of SHAPEIT)
3. Run *SHAPEIT --check* on plink per-chromosome files.
4. Creates snplists from SHAPEIT output. (1) snps2drop.txt - list of SNPs to drop, (2) snp2flip.txt list of SNPs to flip 
5. Adds list of ambiguous SNPs to snps2drop.txt
6. Repeats steps 2-4 until SHAPEIT returns nothing to exclude. This is always completed after 2 rounds, resulting in files containing the string **shaped2x**
7. Uses plink1.9 to convert resulting plink files to VCF files (a requirement of Michigan Imputation Server)



### Who do I talk to? ###

* Repo owner

* Other community or team contact