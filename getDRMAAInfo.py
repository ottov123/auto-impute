#!/usr/bin/env python2.7

import drmaa
import os


def main():
    if not (os.environ.get('DRMAA_LIBRARY_PATH')):
        os.environ['DRMAA_LIBRARY_PATH'] = '/usr/share/sge/sge6.2u5p2/LINUXAMD64_26/lib/lx26-amd64/libdrmaa.so.1.0'




    """ Query the system. """
    with drmaa.Session() as s:
        print('A DRMAA object was created')
        print('Supported contact strings: %s' % s.contact)
        print('Supported DRM systems: %s' % s.drmsInfo)
        print('Supported DRMAA implementations: %s' % s.drmaaImplementation)
        print('Version %d.%d' % s.version)




if __name__ == '__main__':
    main()
