#!/bin/env python2.7
import sys
import os
from subprocess import call
import drmaa

dataPrefix = sys.argv[1]  # e.g. ADC1.hg19

dir1 = '/opt/ALL_1000G_phase1integrated_v3_impute'

try:
    call(['mkdir', '-p', 'byChr'])
    call('/bin/grep -P "[AT]\t[AT]" %s.bim|cut -f2 > %s.ambiguous_snps.txt' % (dataPrefix, dataPrefix), shell=True)
    call('/bin/grep -P "[CG]\t[CG]" %s.bim|cut -f2 >> %s.ambiguous_snps.txt' % (dataPrefix, dataPrefix), shell=True)
except:
    print "Unexpected error:", sys.exc_info()[0]
    raise

with drmaa.Session() as s:
    print('Creating plink job template')
    jt = s.createJobTemplate()
    jt.workingDirectory = os.getcwd()
    jt.outputPath = ':/dev/null'
    jt.joinFiles = True
    jt.remoteCommand = '/usr/local/bin/plink'

    jobids = []
    for x in range(1, 22 + 1):
        jt.jobName = 'plink-chr%s' % x
        jt.args = ['--noweb',
                   '--bfile', dataPrefix,
                   '--exclude', dataPrefix + '.ambiguous_snps.txt',
                   '--make-bed',
                   '--mind', '0.05',
                   '--chr', str(x),
                   '--out', 'byChr/%s.chr%s' % (dataPrefix, x)
                   ]

        jobids.append(s.runJob(jt))

    print('%d jobs submitted' % (len(jobids)))

    print('Holding for completion')
    s.synchronize(jobids, drmaa.Session.TIMEOUT_WAIT_FOREVER, True)

    print('Running SHAPEIT -check')
    jt.remoteCommand = '/usr/local/bin/shapeit'

    jobids = []
    for x in range(1, 22 + 1):

        jt.jobName = 'shapeC-%s.chr%s' % (dataPrefix, x)
        jt.args = ['-check',
                   '-B', 'byChr/%s.chr%s' % (dataPrefix, x),
                   '-M', '%s/genetic_map_chr%s_combined_b37.txt' % (dir1, x),
                   '--input-ref',
                   '%s/ALL_1000G_phase1integrated_v3_chr%s_impute.hap.gz' % (dir1, x),
                   '%s/ALL_1000G_phase1integrated_v3_chr%s_impute.legend.updated.gz' % (dir1, x),
                   '%s/ALL_1000G_phase1integrated_v3.sample' % dir1,
                   '--output-log', 'byChr/%s.chr%s.hhchecks' % (dataPrefix, x)
                   ]

        jobids.append(s.runJob(jt))

    print('%d jobs submitted' % (len(jobids)))

    print('Holding for completion')
    s.synchronize(jobids, drmaa.Session.TIMEOUT_WAIT_FOREVER, True)

    call('grep strand byChr/%s.chr*.hhchecks.snp.strand|cut -f3 |sort|uniq >> byChr/snps2flip.txt' % dataPrefix, shell=True)
    call('grep missing byChr/%s.chr*.hhchecks.snp.strand|cut -f3 >> byChr/snps2drop.txt' % dataPrefix, shell=True)
    call('cat %s.ambiguous_snps.txt >> byChr/snps2drop.txt' % dataPrefix, shell=True)
    call('cat byChr/*.irem | sort | uniq > byChr/irem.irem', shell=True)

    print('Running plink again (shaped-1)')
    jt.remoteCommand = '/usr/local/bin/plink'

    jobids = []
    for x in range(1, 22 + 1):
        jt.jobName = 'plink-shaped-chr%s' % x
        jt.args = ['--noweb',
                   '--bfile', dataPrefix,
                   '--exclude', 'byChr/snps2drop.txt',
                   '--flip', 'byChr/snps2flip.txt',
                   '--make-bed',
                   '--mind','0.05',
                   '--remove', 'byChr/irem.irem',
                   '--chr', str(x),
                   '--out', 'byChr/%s.shaped.chr%s' % (dataPrefix, x)
                   ]

        jobids.append(s.runJob(jt))

    print('%d jobs submitted' % (len(jobids)))

    print('Holding for completion')
    s.synchronize(jobids, drmaa.Session.TIMEOUT_WAIT_FOREVER, True)

    print('Running SHAPEIT -check 2nd')
    jt.remoteCommand = '/usr/local/bin/shapeit'

    jobids = []
    for x in range(1, 22 + 1):

        jt.jobName = 'shapeC-%s.shaped.chr%s' % (dataPrefix, x)
        jt.args = ['-check',
                   '-B', 'byChr/%s.shaped.chr%s' % (dataPrefix, x),
                   '-M', '%s/genetic_map_chr%s_combined_b37.txt' % (dir1, x),
                   '--input-ref',
                   '%s/ALL_1000G_phase1integrated_v3_chr%s_impute.hap.gz' % (dir1, x),
                   '%s/ALL_1000G_phase1integrated_v3_chr%s_impute.legend.updated.gz' % (dir1, x),
                   '%s/ALL_1000G_phase1integrated_v3.sample' % dir1,
                   '--output-log', 'byChr/%s.shaped.chr%s.hhchecks' % (dataPrefix, x)
                   ]

        jobids.append(s.runJob(jt))

    print('%d jobs submitted' % (len(jobids)))

    print('Holding for completion')
    s.synchronize(jobids, drmaa.Session.TIMEOUT_WAIT_FOREVER, True)

    call('cut -f3 byChr/%s.shaped.chr*.hhchecks.snp.strand >> byChr/snps2drop.txt' % dataPrefix, shell=True)
    call('cat byChr/*.irem | sort | uniq > byChr/irem.irem', shell=True)

    print('Running plink again (shaped-2)')
    jt.remoteCommand = '/usr/local/bin/plink'

    jobids = []
    for x in range(1, 22 + 1):
        jt.jobName = 'plink-shaped2x-chr%s' % x
        jt.args = ['--noweb',
                   '--bfile', dataPrefix,
                   '--exclude', 'byChr/snps2drop.txt',
                   '--flip', 'byChr/snps2flip.txt',
                   '--make-bed',
                   '--remove', 'byChr/irem.irem',
                   '--chr', str(x),
                   '--out', 'byChr/%s.shaped2x.chr%s' % (dataPrefix, x)
        ]

        jobids.append(s.runJob(jt))

    print('%d jobs submitted' % (len(jobids)))

    print('Holding for completion')
    s.synchronize(jobids, drmaa.Session.TIMEOUT_WAIT_FOREVER, True)

    print('Running SHAPEIT -check 3rd')
    jt.remoteCommand = '/usr/local/bin/shapeit'

    jobids = []
    for x in range(1, 22 + 1):
        jt.jobName = 'shapeC-%s.shaped2x.chr%s' % (dataPrefix, x)
        jt.args = ['-check',
                   '-B', 'byChr/%s.shaped2x.chr%s' % (dataPrefix, x),
                   '-M', '%s/genetic_map_chr%s_combined_b37.txt' % (dir1, x),
                   '--input-ref',
                   '%s/ALL_1000G_phase1integrated_v3_chr%s_impute.hap.gz' % (dir1, x),
                   '%s/ALL_1000G_phase1integrated_v3_chr%s_impute.legend.updated.gz' % (dir1, x),
                   '%s/ALL_1000G_phase1integrated_v3.sample' % dir1,
                   '--output-log', 'byChr/%s.shaped2x.chr%s.hhchecks' % (dataPrefix, x)
        ]

        jobids.append(s.runJob(jt))

    print('%d jobs submitted' % (len(jobids)))

    print('Holding for completion')
    s.synchronize(jobids, drmaa.Session.TIMEOUT_WAIT_FOREVER, True)

    #
    # convert to vcf using plink1.9
    #

    print('Converting plink files to VCF using plink1.9')
    jt.remoteCommand = '/usr/local/bin/plink1.9'
    #jt.outputPath = ':/tmp/stdout'
    jobids = []
    for x in range(1, 22 + 1):
        jt.jobName = 'VCF-Convert-%s.chr%s' % (dataPrefix, x)
        jt.args = ['--bfile', 'byChr/%s.shaped2x.chr%s' % (dataPrefix, x),
                   '--recode', 'vcf', 'bgz',
                   '--out', 'byChr/%s.shaped2x.chr%s' % (dataPrefix, x),
        ]

        jobids.append(s.runJob(jt))

    print('%d jobs submitted' % (len(jobids)))

    print('Holding for completion')
    s.synchronize(jobids, drmaa.Session.TIMEOUT_WAIT_FOREVER, True)


    # End - cleanup
    s.deleteJobTemplate(jt)

